/* 
Google Drive API

required npm package: googleapis
*/
const { google } = require('googleapis');
const path = require('path');
const fs = require('fs');
require('dotenv').config();

//get from console.cloud.google.com
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URI = process.env.REDIRECT_URI;


//get from https://developers.google.com/oauthplayground/
const REFRESH_TOKEN = process.env.REFRESH_TOKEN;

const oauth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oauth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive = google.drive({
  version: 'v3',
  auth: oauth2Client,
});

  //create folder
  async function createFolder(){
      try {
        const fileMetadata = {
            'name': 'Demo Google Drive', //nama folder
            'mimeType': 'application/vnd.google-apps.folder'
        };
        drive.files.create({
            resource: fileMetadata,
            fields: 'id'
        }, function (err, file) {
            if (err) {
                // Handle error
                console.error(err);
            } else {
                console.log('Folder Id: ', file.id);
            }
        });
    } catch (error) {
        console.log(error.message);
    }
    
}

createFolder()