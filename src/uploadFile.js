/* 
Google Drive API

required npm package: googleapis
*/
const { google } = require('googleapis');
const path = require('path');
const fs = require('fs');
require('dotenv').config();

//get from console.cloud.google.com
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URI = process.env.REDIRECT_URI;


//get from https://developers.google.com/oauthplayground/
const REFRESH_TOKEN = process.env.REFRESH_TOKEN;

const oauth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oauth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive = google.drive({
  version: 'v3',
  auth: oauth2Client,
});

//upload file on root
const filePath = path.join(__dirname, 'test.jpg'); // filename

async function uploadFile() {
    try {
        const response = await drive.files.create({
            requestBody: {
                name: 'test.jpg', //This can be name of your choice
                mimeType: 'image/jpg',
            },
            media: {
                mimeType: 'image/jpg',
                body: fs.createReadStream(filePath),
            },
        });
        console.log(response.data);
    } catch (error) {
        console.log(error.message);
    }
}



uploadFile();