const async = require('async');
const { google } = require('googleapis');
const path = require('path');
const fs = require('fs');
require('dotenv').config();

//get from console.cloud.google.com
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URI = process.env.REDIRECT_URI;

//get from https://developers.google.com/oauthplayground/
const REFRESH_TOKEN = process.env.REFRESH_TOKEN;

const oauth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oauth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive = google.drive({
  version: 'v3',
  auth: oauth2Client,
});

// file/folder id
var fileId = 'YOUR FILE/FOLDER ID';
var permissions = [
  {
    'type': 'user',
    'role': 'writer',
    'emailAddress': 'xxxxx@gmail.com' // email tujuan
  }, {
    'type': 'domain', // nama domain yang akan dishare, jika akan share berdasarkan domain
    'role': 'writer',
    'domain': 'example.com' 
  }
];
function givePermission(){
    async.eachSeries(permissions, function (permission, permissionCallback) {
        drive.permissions.create({
          resource: permission,
          fileId: fileId,
          fields: 'id',
        }, function (err, res) {
          if (err) {
            // Handle error...
            console.error(err);
            permissionCallback(err);
          } else {
            console.log('Permission ID: ', res.id)
            permissionCallback();
          }
        });
      }, function (err) {
        if (err) {
          // Handle error
          console.error(err);
        } else {
          // All permissions inserted
        }
      });
}

givePermission()